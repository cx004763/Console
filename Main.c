#include <stdio.h>
#include <string.h>
#include <time.h>

void start();
void exit_on_return();
void delay(float seconds);
void print(char *text);
void println(char *text);
void backspace(int len);
void newline();

int main(void) {
	/* Please Do Not Edit This Function Or The Other Core Functions, Please Use The start() Function To Add New Code */
	start();
	exit_on_return();
	return 0;
}

void start() {
	print("Executing Program #42");
	print(".");
	delay(0.5);
	print(".");
	delay(0.5);
	print(".");
	delay(0.5);
	print(".");
	delay(0.5);
	print(".");
	delay(2);
	backspace(5);
	println("");
	println("");
	print("Hello World");
	delay(1);
	println("");
	println("");
	print("My Name Is Jonny");
	delay(0.5);
	backspace(5);
	print("Joseph");
	delay(0.5);
	backspace(6);
	print("Kyle");
	delay(0.5);
	backspace(4);
	print("Aston");
	delay(0.5);
	backspace(5);
	print("Antonia");
	delay(0.5);
	backspace(7);
	print("Charlotte");
	delay(0.5);
	backspace(9);
	print("Royce");
	delay(0.5);
	backspace(5);
	print("Sword");
	delay(0.5);
	println("... What's Your Name?");
	println("");
	print("My Name Is ");
	char Name[32];
	gets(Name);
	println("");
	print("Hello ");
	print(Name);
	print(", Nice To Meet You :)");
	delay(2);
	println("");
	println("");
	println("I Hope You Liked This Little Showcase Of What This Addon Can Do :)");
	println("");
}

void exit_on_return() {
	/* Prevents The Console From Automatically Closing - Closes Once The User Presses Enter */
	int test = getchar();
}

void delay(float seconds) {
	/* Pauses The Console For A Specific Amount Of Time In Seconds, It Allows Floats E.G. (2.57) Seconds */
	clock_t StartTick = clock();
	while (clock() - StartTick < CLOCKS_PER_SEC * seconds) {

	}
}

void print(char *text) {
	/* Prints Text To The Console - Prints Similar To How A User Would Type (Letter By Letter)
	Don't Worry It Prints Each Letter Very Quickly, It Makes Reading The Text More Interactive */
	int len = strlen(text);
	for (int x = 0; x < len; x++) {
		printf("%.*s", 1, text + x);
		delay(0.05);
	}
}

void println(char *text) {
	/* Same As print() However It Inserts A New Line After The Text Has Been Printed
	NOTE : backspace() Will Not Work For Text That Has Been Parsed Through This Function */
	print(text);
	newline();
}

void backspace(int len) {
	/* Allows The User To Remove A Specific Amount Of Characters From Printed Text In The Console */
	for (int x = 0; x < len; x++) {
		printf("\b \b");
		delay(0.05);
	}
}

void newline() {
	/* Creates A Break In The Console Allowing Text To Be Printed To A New Line */
	printf("\n");
}